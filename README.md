# README #

This is Jeff's sample implementation of frequent use cases of SAS for proc_groovy. These were done using Groovy Grails Test Suite (GGTS)

### Contains implementation of ###
1. JSON parsing
2. Reading zip files
3. Iterating thru directory files
4. Connecting to Database (MongoDB), and fetching document items
5. Connecting to RESTful serice (using RESTClient) (PENDING)

### Grails implementation to my Linode server still pending due to server issues being fixed by Linode team.