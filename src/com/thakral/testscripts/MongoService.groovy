package com.thakral.testscripts

@Grab('org.mongodb:mongodb-driver:3.2.2')
import com.mongodb.MongoClient
import com.mongodb.DBCollection
import com.mongodb.DB
import com.mongodb.ServerAddress
import com.mongodb.MongoCredential

class MongoService {
	private MongoClient mongoClient 

    def host = "localhost" //your host name 
    def port = 27017 //your port no. 
    def databaseName = 'app1'

    public MongoClient client() {
        mongoClient = mongoClient ?: new MongoClient(host, port) 

        return mongoClient
    } 

    public DBCollection collection(collectionName) { 
        DB db = client().getDB(databaseName)

        return db.getCollection(collectionName) 
    }
	
	static void main(def args){
		def service = new MongoService(databaseName: 'app1')
		def ep = service.collection('extractProcess')
		
		ep.find().toArray().each {
			println it
		}
	}
}
