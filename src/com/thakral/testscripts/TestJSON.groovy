package com.thakral.testscripts

import groovy.json.JsonSlurper

class TestJSON {
	
	/*
	 * Using JSON Slurper, according to SAS Docs.
	 * 
	 * */
	
	static void main(def args){
		def slurper = new JsonSlurper()
		def result = slurper.parseText('{"person":{"name":"Guillaume","age":33,"pets":["dog","cat"]}}')
		
		println(result.person.name)
		println(result.person.age)
		println(result.person.pets.size())
		println(result.person.pets[0])
		println(result.person.pets[1])
	}

}
