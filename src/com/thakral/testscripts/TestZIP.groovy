package com.thakral.testscripts

class TestZIP {
	
	static def dumpZipContent(File zipFIle) {
		def zf = new java.util.zip.ZipFile(zipFIle)
		zf.entries().findAll { !it.directory }.each {
		  println it.name
		  println zf.getInputStream(it).text
		}
	  }
	
	static void main(def args){
		dumpZipContent(new File('/Users/jeffreybarron/Desktop/sample.zip'))
	}

}
