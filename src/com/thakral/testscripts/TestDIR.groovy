package com.thakral.testscripts;

import groovy.io.FileType

public class TestDIR {

	static void main(def args){
		def list = []

		def dir = new File("/Users/jeffreybarron/Desktop")
		dir.eachFileRecurse (FileType.FILES) { file -> list << file }

		list.each { println it.path }
	}
}
